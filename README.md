# GreaseMonkey - Hanbitmedia purchased ebook marking
한빛미디어에서 구매한 전자책인 경우 표시

[Source](https://bitbucket.org/hwiorn/gm-hanbit-purchased-ebook/src/master/GreaseMonkey_hanbit_bought_ebook_marking.user.js?fileviewer=file-view-default)

[Install](https://bitbucket.org/hwiorn/gm-hanbit-purchased-ebook/raw/master/GreaseMonkey_hanbit_bought_ebook_marking.user.js)

