// ==UserScript==
// @name        Hanbitmedia purchased ebook marking
// @namespace   https://bitbucket.org/hwiorn/gm-hanbit-purchased-ebook
// @description 한빛미디어에서 구매한 전자책인 경우 표시
// @include     *hanbit.co.kr/ebook/*
// @icon http://www.hanbit.co.kr/img/common/hanbit.ico
// @version     1.2
// @updateURL https://bitbucket.org/hwiorn/gm-hanbit-purchased-ebook/raw/master/GreaseMonkey_hanbit_bought_ebook_marking.user.js
// @downloadURL https://bitbucket.org/hwiorn/gm-hanbit-purchased-ebook/raw/master/GreaseMonkey_hanbit_bought_ebook_marking.user.js
// @grant       GM_getValue
// @grant       GM_setValue
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// ==/UserScript==

function _tbought(e) {
  e.prepend('[구매완료]');
  e.css('color', '#999');
  e.css('text-decoration', 'line-through');
}
function _mark_b(el) {
  var thumb = el.find('img');
  var p = el.find('p');
  if (thumb.length > 0) {
    thumb.css('opacity', '0.4');
    thumb.css('filter', 'alpha(opacity=40)');
    if (p.length > 0)
    _tbought(p);
  } else if (el.text().length > 0 &&
    el.text().search('>') < 0 &&
    el.text().search('<') < 0) {
    _tbought(el);
  }
}
$('a[href*=\'look.html\']').each(function (i, e) {
  var el = $(e);
  var isbn = /isbn=(\d+)/.exec(el.attr('href')) [1];
  var book = GM_getValue(isbn);
  if (book == 'bought') {
    _mark_b(el);
  } else {
    $.ajax({
      'type': 'GET',
      'url': '/my_hanbit/ebook_download.html',
      success: function (d) {
        var r = new RegExp('getMyEbook\\(.+,\'' + isbn + '\'', 'g');
        r.exec(d).forEach(function (e, i, a) {
          GM_setValue(isbn, 'bought');
          _mark_b(el);
        });
      },
    });
  }
});
